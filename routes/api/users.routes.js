const usersController = require('../../controllers/users.controller');
const express = require('express');
const router = express.Router();

/**
 * Create a user.
 */
router.post('/createUser', function (req, res) {
    usersController.createUser(req.body, res);
});

/**
 * Update user record.
 */
router.put('/updateUser', function (req, res) {
    usersController.updateUser(req.body, res);
});

/**
 * Delete user record.
 */
router.delete('/deleteUser/:userID', function (req, res) {
    usersController.deleteUser(req.params.userID, res);
});

/**
 * Get a user by login details.
 */
router.post('/loginUser', function (req, res) {
    usersController.getUserLogin(req.body, res);
});

/**
 * Get all users.
 */
router.get('/users', function (req, res) {
    usersController.getUsers(res);
});

/**
 * Get user by specified user ID.
 */
router.get('/users/:userID', function (req, res) {
    usersController.getUserByID(req.params.userID, res);
});

module.exports = router;