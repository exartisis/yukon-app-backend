const usersModel = require('../models/users.model');

const _usersModel = new usersModel();

/**
 * Users controller.
 * @author Roshan Gerard Bolonna
 */
class UsersController {

    static createUser(model, res) {
        _usersModel.createUser(model, null).then(data => {
            res.json(data);
        }).catch(err => {
            res.json(err);
        });
    }

    static updateUser(model, res) {
        _usersModel.updateUser(model, null).then(data => {
            res.json(data);
        }).catch(err => {
            res.json(err);
        });
    }

    static deleteUser(model, res) {
        _usersModel.deleteUser(model, null).then(data => {
            res.json(data);
        }).catch(err => {
            res.json(err);
        });
    }

    static getUserLogin(model, res) {
        _usersModel.getUserLogin(model, (err, result) => {
            if(err) res.send(err);

            res.json(result);
        });
    }

    static getUsers(res) {
        _usersModel.getUsers((err, result) => {
            if (err) res.send(err);

            res.json(result);
        });
    }

    static getUserByID(userID, res) {
        _usersModel.getUserByID(userID, (err, result) => {
            if (err) res.send(err);

            res.json(result[0]);
        });
    }
}

module.exports = UsersController;