const mysql = require('mysql');
const mysqlConfig = require('../../configs/configs.db.mysql');

/**
 * Create mysql database connections and other related functions.
 * @author Roshan Gerard Bolonna
 */
class MySQLDBCon {

    /**
     * Create mysql db connection and return
     * @return {Connection} - returns if successful connection.
     */
    createConnection() {
        const connection = mysql.createConnection({
            host: mysqlConfig.DB_MYSQL_SETTINGS.HOST,
            database: mysqlConfig.DB_MYSQL_SETTINGS.DATABASE,
            user: mysqlConfig.DB_MYSQL_SETTINGS.USER,
            password: mysqlConfig.DB_MYSQL_SETTINGS.PASSWORD
        });

        connection.connect((err) => {
            if (err) throw err;
        });

        return connection;
    }
}

module.exports = MySQLDBCon;