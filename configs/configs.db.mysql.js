const dbMySQLSettings = {
    HOST: 'localhost',
    PORT: '3306',
    DATABASE: 'db-ykn',
    USER: 'root',
    PASSWORD: ''
};

module.exports.DB_MYSQL_SETTINGS = dbMySQLSettings;