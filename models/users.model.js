const mysqlDBCon = require('../utils/db/utils.mysql.dbcon');

/**
 * Users table CRUD operations.
 * @author Roshan Gerard Bolonna
 */
class UsersModel {

    constructor() {
        this.mysqlCon = new mysqlDBCon().createConnection();
    }

    createUser(model, result) {
        // try {
        //     this.mysqlCon.query("INSERT INTO users SET ?",
        //         model,
        //         function (err, res) {
        //             if (err) {
        //                 console.log("Error: ", err);
        //                 result(err, null);
        //                 return; // terminate.
        //             }
        //
        //             result(null, res.insertId);
        //         }
        //     );
        // } finally {
        //     this.mysqlCon.end();
        // }

        return new Promise((resolve, reject) => {
            // Prevent passing user ID, causing duplicate id runtime crash.
            model.userID = undefined;

            this.mysqlCon.query("INSERT INTO users SET ?",
                model,
                function (err, res) {
                    if (err) {
                        console.log("Error: ", err);
                        reject(err);
                    } else {
                        resolve(res.insertId)
                    }
                }
            );
        });
    }

    updateUser(model, result) {
        return new Promise((resolve, reject) => {
           this.mysqlCon.query("UPDATE users SET firstName = ?, lastName = ?, userName = ?, password = ? WHERE userID = ?",
               [model.firstName, model.lastName, model.userName, model.password, model.userID],
               function (err, res) {
                   if (err) {
                       console.log("Error: ", err);
                       reject(err);
                   } else {
                       resolve(res.affectedRows);
                   }
               }
           )
        });
    }

    deleteUser(userID, result) {
        return new Promise((resolve, reject) => {
            this.mysqlCon.query("DELETE FROM users WHERE userID = ?",
                [userID],
                function (err, res) {
                    if (err) {
                        console.log("Error: ", err);
                    } else {
                        resolve(res.affectedRows);
                    }
                }
            );
        });
    }

    getUserLogin(model, result) {
        try {
            this.mysqlCon.query('SELECT * FROM users WHERE userName LIKE ? AND password LIKE ?',
                [model.userName, model.password],
                function (err, res) {
                    if (err) {
                        console.log("Error: ", err);
                        result(err, null);
                        return; // terminate.
                    }

                    result(null, res)
                }
            );
        } finally {
            // Doesn't require to end only initialize once.
            // this.mysqlCon.end();
        }
    }

    getUsers(result) {
        try {
            this.mysqlCon.query('SELECT * FROM users',
                null,
                function (err, res) {
                    if (err) {
                        console.log("Error: ", err);
                        result(err, null);
                        return; // terminate.
                    }

                    result(null, res)
                }
            );
        } finally {
            // Doesn't require to end only initialize once.
            // this.mysqlCon.end();
        }
    }

    getUserByID(userID, result) {
        try {
            this.mysqlCon.query('SELECT * FROM users WHERE userID = ?',
                [userID],
                function (err, res) {
                    if (err) {
                        console.log("Error: ", err);
                        result(err, null);
                        return; // terminate.
                    }

                    result(null, res)
                }
            );
        } finally {
            // Doesn't require to end only initialize once.
            // this.mysqlCon.end();
        }
    }
}

module.exports = UsersModel;