const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');

// APIs routes.
const userAPIRouter = require('./routes/api/users.routes');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Allow CORS policy regulations.
app.use(function (req, res, next){
    // Allow specific sites defined here.
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Allow specific REST HTTP methods.
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');

    // Allow sepcific REST HTTP content types.
    res.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, content-type, Content-Type, Accept, body');

    // Allow website to include cookies in the requests sent.
    res.setHeader('Access-Control-Allow-Credentials', false);

    // Proceed to next layer of the middlewear.
    next();
});

app.use('/', indexRouter);
app.use('/users', usersRouter);

// APIs
app.use('/api', userAPIRouter);

module.exports = app;
